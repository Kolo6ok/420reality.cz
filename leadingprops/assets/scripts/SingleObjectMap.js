(function($) {
    "use strict";

    window.lpw = window.lpw || {};

    function SingleObjectMap() {
        var $this = this,
            contact = new window.lpw.ContactForm(true);
        this.singleMap = new window.lpw.Map(
            '#single-map',
            'single',
            new window.lpw.AutoComplete(null, null, 'single', this)
        );

        this.setTotalCounter = function(count) {
          $('#total-counter').text(count);
        };

        this.setEventListeners();
    }
    SingleObjectMap.prototype.setEventListeners = function() {
        var $this = this;
        $('body').on('click.lprop', '.tag-remove', function(ev) {
            ev.preventDefault();
            $this.singleMap.mapReset();
            $this.onClusterSelect(null, true);
        });
        $('.btn-map-open').on('click', function(ev) {
            $this.resizeMap(ev, $this);
        });
    };
    SingleObjectMap.prototype.onClusterSelect = function(data, remove) {
        var locations = (data && data.location) ? data.location : null;
        if(remove) {
            $('.location-title').remove();
        } else {
            var locationTitle = $('.location-title-wrapper');
            if (locationTitle.length > 0) {
                locationTitle.html('<span>' + data.text + '</span>&nbsp;<span class="tag-remove text-red"></span>');
            } else {
                var title = $('<h3 class="location-title">' +
                    '<span class="location-title-wrapper">' +
                    '<span>' + data.text + '</span>&nbsp;<span class="tag-remove text-red"></span>' +
                    '</span>' +
                    '</h3>');
                title.insertAfter('.single-title');
            }
            $('html, body').stop().animate({
                scrollTop: 0
            }, 1000);
        }
        this.getSubTypes(locations)
            .done(this.getSubTypesSuccess.bind(this))
            .fail(this.getSubTypesError.bind(this));

    };
    SingleObjectMap.prototype.getSubTypes = function(location) {
        var data = {
            action: 'do_ajax',
            fn: 'get_subtypes',
            subtype_parent_id: 3
        };
        if(location) {
            if(!location.location_shape && location.location_point) {
                data.location_point = location.location_point;
            } else if(location.location_shape) {
                data.location_shape = location.location_shape;
            }
        }
        return $.ajax({
            url: LpData.ajaxUrl,
            method: 'post',
            dataType: 'json',
            data: data
        });
    };
    SingleObjectMap.prototype.getSubTypesSuccess = function(data) {
        var html = '',
            answer = [],
            rawTypesArray = data.counters,
            tagsList = $('.comm-tags'),
            TypesOrder = [
                'hotel',
                'residential',
                'office_building',
                'retail',
                'shopping_centre',
                'cbr',
                'mixed',
                'other'
            ];

        _.forEach(TypesOrder, function(typeName){
            var target = _.find(rawTypesArray, ['name', typeName]);
            if(target && target.count){
                answer.push(target);
            }
        });

        this.setTotalCounter(data.total);
        _.forEach(answer, function(value) {
            html += '<li><span>' + value.title + ' <sup class="text-red">' + value.count + '</sup></span></li>';
        });
        if(html !== '') {
            if(tagsList.length > 0 ) {
                tagsList.html(html);
            } else {
                $('<ul class="comm-tags">' + html + '</ul>').appendTo('.comm-header');
            }
        } else {
            tagsList.remove();
        }

        tagsList.html(html);
    };
    SingleObjectMap.prototype.getSubTypesError = function(error) {
        console.debug('getSTError', error.responseText);
    };
    SingleObjectMap.prototype.resizeMap = function(ev, $this) {
        ev.preventDefault();
        var maxHeight = 1000,
            map = $('#single-map'),
            mapOffset = map.offset().top,
            mapCenter = $this.singleMap.map.getCenter(),
            windowHeight = $(window).height(),
            btn = $(ev.currentTarget),
            action = btn.data('action'),
            normalHeight = 255,
            normalHeightBtn = normalHeight - 45,
            height = windowHeight < maxHeight ? windowHeight : maxHeight,
            heightBtn = height-45;
        if(action === 'open') {
            btn.addClass('opened').data('action', 'close');
            map.animate({height:height}, 600, 'linear', function() {
                google.maps.event.trigger($this.singleMap.map, "resize");
                $this.singleMap.map.setCenter(mapCenter);
            });
            // btn.css('top', heightBtn);
            $('html, body').stop().animate({
                scrollTop: mapOffset
            }, 600);
        } else {
            btn.removeClass('opened').data('action', 'open');
            map.css('height', normalHeight);
            // btn.css('top', normalHeightBtn);
            google.maps.event.trigger($this.singleMap.map, "resize");
            $this.singleMap.map.setCenter(mapCenter);
        }
    };
    window.lpw.SingleObjectMap = SingleObjectMap;
})(jQuery);
