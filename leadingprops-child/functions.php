<?php
/*function child_theme_enqueue_styles() {

  $parent_style = 'leadingprops';
    
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
  
  wp_enqueue_style( 'child-style',
      get_stylesheet_directory_uri() . '/style-child.css', // http://realty123.rus/wp-content/themes/leadingprops-child/style-child.css
      array( $parent_style ),
      wp_get_theme()->get('Version')
  );
}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_styles' );*/


add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', PHP_INT_MAX);

function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style-child.css', array( 'parent-style' ) );
}

//add_action('wp_enqueue_scripts', 'child_theme_script_fix', PHP_INT_MAX);

function child_theme_script_fix()
{
    wp_dequeue_script('lprop/js');
    wp_deregister_script('lprop/js');
    wp_enqueue_script('lprop/js', get_stylesheet_directory_uri().'/dist/scripts/main.js', [
        'jquery',
        'lodash',
        'google-map',
        'js-marker-clusterer'
    ]);
}

?>
